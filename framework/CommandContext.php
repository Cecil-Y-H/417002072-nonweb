<?php

namespace MOOC\framework;

class CommandContext extends CommandContextAbstract
{


    public function add(string $key, $val)
    {
        $this->data[$key] = $val;
    }

    public function get(string $key)
    {
        //echo "Command Context Get Method Invoked!";
        if(isset($this->data[$key]))
        {
            //echo "the data array is set with a variable";
            return $this->data[$key];
        }
        return null;
    }

    public function getErrors(): array
    {
        return [];
    }

    protected function setError($error)
    {
        array_push($errors, $error);
    }

}