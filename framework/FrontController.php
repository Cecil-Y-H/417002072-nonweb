<?php

namespace MOOC\framework;

use Exception;

class FrontController extends FrontControllerAbstract
{
    public function __construct()
    {
        //forces you to use the run method
    }

    private function __clone()
    {

    }

    public static function run()
    {
        $controller = new FrontController();
        $controller->init();
        $controller->handleRequest();

       
    }

    /**
     * Use this method to initialize helper objects
     * Session Manager, Validator, Response Handler, etc
     * They should be globally available objects. Initialize them here so they can be used elsewhere.
     */
    protected function init()
    {
        $session = SessionClass::getInstance();
        
        $validate = Validation::getInstance();

        $response = ResponseHandler::getInstance();
       
       
    }

    protected function handleRequest()
    {   error_reporting (E_ALL ^ E_NOTICE);
       
        $page = "";

        $context = new CommandContext();
        
        $middle = $context->get('get');
        //var_dump($middle);
        $newRequest = $middle['controller'];
       

        if(empty($newRequest))
        {
            $page = 'index';
        }

        else
        {
            $page = $newRequest;
        }
        

        $handler = RequestHandlerFactory::makeRequestHandler($page);
        
        echo "<br>";
          
        $response = ResponseHandler::getInstance();
        $session = SessionClass::getInstance();
        $session->create();


        if($handler->execute($context) === false)
        {
            echo "Error! Incorrect Request Passed!";
            
            $head =  new WarningHeader();
            $state = new WarningState();
            $logger = new WarningLogger();

            $set = array("Invalid-Request");
            $head->setEntries($set);

            $set = array("An Invalid Request was sent to the Front Controller");
            $state->setEntries($set);

            $time = date("h:i:sa");
            $set = array($time);
            $logger->setEntries($set);
            
            $response->create($head, $state, $logger);

            $session->add("HEADER", $head->getEntry(0));
            $session->add("STATE", $state->getEntry(0));
            $session->add("LOGGER", $logger->getEntry(0));

        }


      

        $head = new NoticeHeader();
        $state = new NoticeState();
        $logger = new NoticeLogger();

        $set = array("Valid-Request");
        $head->setEntries($set);

        $set = array("A Valid Request was sent to the Front Controller");
        $state->setEntries($set);

        $time = date("h:i:sa");
        $set = array($time);
        $logger->setEntries($set);

        $response->create($head, $state, $logger);

        //echo $head->getEntry(0); echo "<br><br>";
        //echo $state->getEntry(0); echo "<br><br>";
        //echo $logger->getEntry(0);

        $session->add("RESPONSE", $response);

        /*
        $SESS = $session->see("RESPONSE");
        $head = $SESS->giveHeader();
        echo $head->getEntry(0);

        $state = $SESS->giveState();
        echo $state->getEntry(0);

        $logger = $SESS->giveLogger();
        echo $logger->getEntry(0);
        */

        

       




        return true;
    }

}



