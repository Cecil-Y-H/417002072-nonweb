<?php

namespace MOOC\framework;

class RequestHandlerFactory implements RequestHandlerFactoryInterface
{
    //public static function makeRequestHandler(string $request='index') : PageControllerCommandAbstract

    public static function makeRequestHandler(string $request) : PageControllerCommandAbstract
    {
        //echo $request;
        if (preg_match('/\W/', $request)) 
        {
            throw new \Exception("illegal characters in request");
            
        }
            
        $class = "MOOC\\apps\\" . UCFirst(strtolower($request)) . "Controller";
        
       
        //echo $class;
       

        if (!class_exists($class)) 
        {
            throw new \Exception("No Controller class '$class' located");
        }
            
        $cmd = new $class(); // the receiver can go here
        
        return $cmd;
            
    }
}