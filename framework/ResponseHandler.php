<?php

namespace MOOC\framework;

class ResponseHandler extends Singleton_Class implements ResponseHandlerInterface 
{
    protected $body = [];

    public function __construct()
    {
        
    }


    public function create(ResponseHeader $head, ResponseState $state, ResponseLogger $logger)
    {
        $this->body['header'] = $head;
        $this->body['state'] = $state;
        $this->body['logger'] = $logger;
    }

     // returns response header
     public function giveHeader(): ResponseHeader
     {
         return clone $this->body['header'];
     }

     // returns response state
    public function giveState() : ResponseState
    {
        return clone $this->body['state'];
    }

     // returns response logger
    public function giveLogger() : ResponseLogger
    {
        return clone $this->body['logger'];
    }
}