<?php
namespace MOOC\framework;

class View implements ObserverInterface
{
    private $tpl = '';
    private $data = [];

    public function setTemplate(string $filename)
    {
        if(!file_exists($filename))
        {
            trigger_error('Invalid template given (' . $filename . ') No such file found', E_USER_ERROR);
            exit;
        }

        $this->tpl = $filename;
    }

    public function addVar(string $name, $value)
    {   /*
        if (!preg_match('/^[a-zA-Z_\x80-\xff][a-zA-Z0-9_\x80-\xff]*$/', $name) == 0) {
            trigger_error('Invalid variable name received', E_USER_ERROR);
        }
		*/
        
        
        $this->data[$name] = $value;
    }

    public function display()
    {
        extract($this->data);
        require $this->tpl;
    }

    public function update(ObservableModel $obs)
    {
        $records = $obs->giveUpdate();
        foreach ($records as $k=>$r)
        {
            $this->addVar($k, $r);
        }
        $this->display();
    }

    
	//Please note that these functions are STRICTLY for testing purposes only.
	//They are never used in the framework proper.

	public function getData()
	{
		return $this->data;
	}
	
	public function getTPL()
	{
		return $this->tpl;
	}

}