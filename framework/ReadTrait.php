<?php

namespace MOOC\framework;

trait ReadTrait
{
    /**
     * Method that reads from the mooc Database when invoked.
     */
    abstract public function read(string $id) : array;

}