<?php

namespace MOOC\framework;

use mysqli;

abstract class Model
{
    protected mysqli $connection;

    protected $cached_json = [];

    abstract public function getAll() : array;

    //abstract public function getAll(mysqli $connection) : array;

    abstract public function getRecord(string $id): array;

    public function loadData(string $file) : array
    {
        $filename = basename($file, '.json');

        if(!isset($this->cached_json[$filename]) || empty($this->cached_json[$filename]))
        {
            $json_file = file_get_contents($file);
            $this->cached_json[$filename] = json_decode($json_file, true);
        }

        //return [];
        return $this->cached_json[$filename];
    }


    //Use this function to connect to the mooc database in PHPMyAdmin
    public static function makeConnection() : mysqli
    {
        $dbhost = "localhost";
        $dbuser = "root";
        $dbpass = '';
        $db = "mooc";

        
        $connection = new mysqli($dbhost, $dbuser, $dbpass, $db);

        if($connection->connect_error)
        {
            die("Database Connection Failed: ". $connection->connect_error);
        }

        else
            //echo "Database Connection Successful!";

        //var_dump($connection);

        return $connection;
    }
}