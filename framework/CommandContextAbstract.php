<?php

namespace MOOC\framework;

abstract class CommandContextAbstract
{
    // Stores data for the context.
    protected $data = [];

    // Stores error messages
    protected $errors = [];

    /**
     * Initializes the command data property with the 
     * data stored in the POST and GET superglobals. Has a param
     * subarray for user declared variables.
     */

    public function __construct()
    {
        //echo "Command Context Abstract Constructor Method Invoked!"; echo "<br>";
        $this->data['post'] = $_POST;
        $this->data['get'] = $_GET;
        $this->data['params'] = [];

        //var_dump($this->data);
        //var_dump($this->data['get']);
       //$test = $this->data['get'];
        //echo $test['controller'];
    }

    // adds new variable to context in the param subarray
    abstract public function add(string $key, $val);

    // gets stored variables.
    abstract public function get(string $key);

    // sets errors
    abstract protected function setError($error);

    //returns all error messages set in the context
    abstract public function getErrors(): array;

}