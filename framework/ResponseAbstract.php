<?php

namespace MOOC\framework;

abstract class ResponseAbstract
{
    protected $data = [];

    // showAllData in the videos
    public function getAllData(string $sep='<br>') : string
    {
        return implode($sep, $this->data);
    }

    // addEntries in the videos
    abstract public function setEntries(array $entries) : bool;

    // showEntry in the videos.
    abstract public function getEntry(int $i) : string;

    // showEntries in the videos
    abstract public function getEntries(int $start, int $end) : string;

}