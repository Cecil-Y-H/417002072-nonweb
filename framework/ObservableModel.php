<?php

namespace MOOC\framework;

abstract class ObservableModel extends Model implements ObservableInterface
{
    protected $observers = [];
     
    protected $updateddata = [];

    public function attach(ObserverInterface $ob)
    {
        $this->observers[] = $ob;
    }

    public function detach(ObserverInterface $ob)
    {
        $this->observers = array_filter(
            $this->observers, function ($a) use ($ob) {
                                return (! ($a === $ob));
                            });
    }

    public function notify()
    {
        foreach ($this->observers as $ob)
        {
            $ob->update($this);
        }
    }

    public function giveUpdate()
    {
        return $this->updateddata;
    }    

    public function updateTheChangedData(array $d)
    {
        $this->updateddata = $d;
    }

    abstract public function getAll() : array;

    //abstract public function getAll(mysqli $connection) : array;

    abstract public function getRecord(string $id) : array;


}