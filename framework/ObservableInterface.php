<?php

namespace MOOC\framework;

interface ObservableInterface
{
    public function attach(ObserverInterface $ob);

    public function detach(ObserverInterface $ob);

    public function notify();


}