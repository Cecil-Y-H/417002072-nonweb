<?php

namespace MOOC\framework;

class SessionClass extends Singleton_Class
{

    //protected $access = ['profile'=>['testuser', 'John Doe']];
    
	
	//array containing the emails of the people who have been registered.
	protected $access = ['profile' => 
							[	'testuser',	
								'tester@comp3170.com',
								'EmailAddress@hotmail.com',
								'exampleaddress@gmail.com',
								'AliceWilson@gmail.com',
								'DavidSamson@yahoo.com',	
								'bobby@gmail.com'
							]
						];
	
    
    public $newAccess = ['profile' =>
    [

    ]
    ]
    ;




	

    public static function create()
    {
        /**
         * When the Session Manager is created, the database is read and emails from registered users are placed into the access array
         */
		session_start();
		 
        $connection = ObservableModel::makeConnection();
        $test = "select * from users";

        $result = $connection->query($test);

        if($result->num_rows > 0)
        {
            $x = 0;
            while($row = $result->fetch_assoc())
            {
                $newAccess['profile'][$x] = $row['email'];
                $x++;
            }
        }

        //var_dump($newAccess);
        //echo $newAccess['profile'][0];
        echo "<br>";
        

       
    }

    public static function destroy()
    {
        session_destroy();
		setcookie("PHPSESSID", "", time() - 3600);
    }    

    public static function add($name, $value)
    {
        if (!preg_match('/^[a-zA-Z_\x80-\xff][a-zA-Z0-9_\x80-\xff]*$/', $name)) {
            trigger_error('Invalid variable name received', E_USER_ERROR);
        }       

        $_SESSION[$name] = $value;
		//echo $_SESSION[$name];
    }

    public function see($name)
    {	
        if(isset($_SESSION[$name]))
        {	
            return $_SESSION[$name];
        }

        return null;
    }

    public function accessible($user, $page) : bool
    {
        //echo $page; echo "<br><br>";
        //echo $user; echo "<br><br>";
        /*
        if(in_array($user, $this->newAccess[$page]))
        {
             return true;
        }
		*/
        
        $data = $this->getAccessArray();
        //var_dump($data);

        if(in_array($user, $data['profile']))
        {
            return true;
        }

        return false;
    }

    public function remove(string $name)
    {
        if(isset($_SESSION[$name]))
        {
            unset($_SESSION[$name]);
        }
    }
	

    



    public function getAccessArray() : array
    {
        $connection = ObservableModel::makeConnection();
        $test = "select * from users";

        $result = $connection->query($test);

        if($result->num_rows > 0)
        {
            $x = 0;
            while($row = $result->fetch_assoc())
            {
                $newAccess['profile'][$x] = $row['email'];
                $x++;
            }
        }


        return $newAccess;
    }

}