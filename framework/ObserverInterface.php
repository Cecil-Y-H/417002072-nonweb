<?php

namespace MOOC\framework;

Interface ObserverInterface
{
    public function update(ObservableModel $obs);
}