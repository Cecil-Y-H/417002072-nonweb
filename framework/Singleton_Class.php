<?php

namespace MOOC\framework;

class Singleton_Class
{
    //array for storing each subclass of the singleton such as the 
    //Session Manager and Validator.
    private static $instances = [];
    
    protected function __construct()
    {

    }

    // Abstract class to get an instance of the specific class.
    public function __wakeup()
    {
        //throw new \Exception("Cannot unserialize singleton");
    }
    

    public static function getInstance()
    {
        $subclass = static::class;
        if(!isset(self::$instances[$subclass]))
        {
            self::$instances[$subclass] = new static();
        }


        return self::$instances[$subclass];
    }
    
    
    
}