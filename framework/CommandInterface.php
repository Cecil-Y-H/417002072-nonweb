<?php

namespace MOOC\framework;

interface CommandInterface
{
    public function execute(CommandContext $context) : bool;
}