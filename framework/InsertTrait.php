<?php

namespace MOOC\framework;

trait InsertTrait
{
    /**
     * Takes data as name/value pairs in an array where
     * name : associate array index e.g data['eg'] = "example"
     * 
     */
    abstract public function insert(array $values);
}