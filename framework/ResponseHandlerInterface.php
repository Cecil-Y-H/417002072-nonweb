<?php

namespace MOOC\framework;

interface ResponseHandlerInterface
{
    // returns response header
    public function giveHeader(): ResponseHeader;

    // returns response state
    public function giveState() : ResponseState;

    // returns response logger
    public function giveLogger() : ResponseLogger;



}