<?php
// This function searches the directories specified below
// for the class/interface/abstract class passed to the autoloader.
// phpunit --bootstrap ./autoload.php tests

//require_once 'PHPUnit/Autoload.php';

//require_once ('PHPUnit/Framework/TestCase.php');

namespace MOOC\tests;

use MOOC\framework\ResponseHandler;
use MOOC\framework\ResponseHeader;
use MOOC\framework\ResponseState;
use MOOC\framework\ResponseLogger;

use PHPUnit\Framework\TestCase;

class ResponseHandlerTest extends TestCase
{
    public function testResponseHandlerCreated() : void
    {

        $testobject = ResponseHandler::getInstance();
        $this->assertIsObject($testobject);
    }

   
    public function testGiveHeader()
    {
        $testobject = ResponseHandler::getInstance();
        $head = new ResponseHeader();
        $state = new ResponseState();
        $logger = new ResponseLogger();

        $set = array("Test-Header");
        $head->setEntries($set);

        $set = array("Response Description");
        $state->setEntries($set);
    
        $time = date("h:i:sa");
        $set = array($time);
        $logger->setEntries($set);

        $testobject->create($head, $state, $logger);

        $this->assertNotNull($testobject->giveHeader());
    }

    public function testGiveState()
    {
        $testobject = ResponseHandler::getInstance();
        $head = new ResponseHeader();
        $state = new ResponseState();
        $logger = new ResponseLogger();

        $set = array("Test-Header");
        $head->setEntries($set);

        $set = array("Response Description");
        $state->setEntries($set);
    
        $time = date("h:i:sa");
        $set = array($time);
        $logger->setEntries($set);

        $testobject->create($head, $state, $logger);

        $this->assertNotNull($testobject->giveState());
    }

    public function testGiveLogger()
    {
        $testobject = ResponseHandler::getInstance();
        $head = new ResponseHeader();
        $state = new ResponseState();
        $logger = new ResponseLogger();

        $set = array("Test-Header");
        $head->setEntries($set);

        $set = array("Response Description");
        $state->setEntries($set);
    
        $time = date("h:i:sa");
        $set = array($time);
        $logger->setEntries($set);

        $testobject->create($head, $state, $logger);

        $this->assertNotNull($testobject->giveLogger());
    }
}