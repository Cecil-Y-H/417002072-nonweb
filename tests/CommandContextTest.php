<?php

use PHPUnit\Framework\TestCase;

use MOOC\framework\CommandContext;


class CommandContextTest extends TestCase
{
    public function testCommandContextCreated()
    {
        $testobject = new CommandContext();
        $this->assertIsObject($testobject);
    }

    
    public function testAdd_and_GetMethod()
    {
        $testobject = new CommandContext();
        $testobject->add('example', 1);
        $result = $testobject->get('example');
        $this->assertEquals($result, "1");
    }

}
