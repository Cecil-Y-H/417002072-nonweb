<?php

use PHPUnit\Framework\TestCase;

use MOOC\framework\Singleton_Class;

class SingletonTest extends TestCase
{
    public function testAreObjectsSingleton()
    {
        $test1 = Singleton_Class::getInstance();
        $test2 = Singleton_Class::getInstance();

        $this->assertTrue($test1 === $test2);

    }


}