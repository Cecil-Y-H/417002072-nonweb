<?php

use PHPUnit\Framework\TestCase;

use MOOC\framework\SessionClass;


class SessionTest extends TestCase
{
    public function testSessionObjectIsCreated() : void
    {
		$testObject = SessionClass::getInstance();
        $this->assertIsObject($testObject);
    }



    public function testSessionAdd()
	{
		$testObject = SessionClass::getInstance();
		$name = 'test';
		$value = 'testvalue';
		//$testObject->create();
		$testObject->add($name, $value);
		//echo $_SESSION[$name];
		$this->assertTrue($_SESSION['test'] == 'testvalue');
	}

}


