<?php

use PHPUnit\Framework\TestCase;

//require 'TestingController.php';
use MOOC\apps\IndexController;
use MOOC\framework\CommandContext;

class ControllerTest extends TestCase
{
	
	public function testControllerObjectCreated()
	{
		$testObject = new IndexController();
		$this->assertIsObject($testObject);
	}
	
	
	public function testCreateModel()
	{
		$testObject = new IndexController();
		$testModel = $testObject->CreateModel();
		$this->assertIsObject($testModel);
	}
	
	public function testCreateView()
	{
		$testObject = new IndexController();
		$testView = $testObject->CreateView();
		$this->assertIsObject($testView);
	}
	
}	
	
	
	



