<?php

use PHPUnit\Framework\TestCase;

//require 'app/IndexModel.php';
//require 'autoload.php';

use MOOC\apps\IndexModel;

class ModelTest extends TestCase
{
    public function testModelObjectCreated() : void
    {
		$testobject = new IndexModel();
		$this->assertIsObject($testobject);
    }
	
	
	public function testGetAll()
	{
		$testObject = new IndexModel();
		$this->assertIsArray($testObject->getAll());
	}
	

	public function testGetRecord()
	{
		$testObject = new IndexModel();
		$array = $testObject->getRecord('name');
		$this->assertEmpty($array);
	}

}