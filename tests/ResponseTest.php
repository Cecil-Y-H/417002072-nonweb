<?php

use PHPUnit\Framework\TestCase;

use MOOC\framework\ResponseHeader;
use MOOC\framework\ResponseState;
use MOOC\framework\ResponseLogger;

class ResponseTest extends TestCase
{
    public function testHeaderSet_and_Get_Entries()
    {
        $testobject = new ResponseHeader();
        $testarray = array("Example Array");
        $testobject->setEntries($testarray);
        $this->assertIsString($testobject->getEntry(0));

    }

    
    public function testStateSet_and_Get_Entries()
    {
        $testobject = new ResponseState();
        $testarray = array("Example Array");
        $testobject->setEntries($testarray);
        $this->assertIsString($testobject->getEntry(0));

    }


    public function testLoggerSet_and_Get_Entries()
    {
        $testobject = new ResponseLogger();
        $testarray = array("Example Array");
        $testobject->setEntries($testarray);
        $this->assertIsString($testobject->getEntry(0));

    }

   
    

}