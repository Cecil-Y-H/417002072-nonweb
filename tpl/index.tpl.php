<!DOCTYPE html>
<html lang="en-GB">
	<head>
		<title>Quwius</title>
		<link rel="stylesheet" href="css/styles.css" type="text/css" media="screen">
		<meta charset="utf-8">
	</head>
	<body>

	<?php
		//var_dump($RecomImage);
	?>
			

	

		<nav> 
			<a href="index.php?controller=index"><img src="images/logo.png" alt="Quwius"></a> 
			<ul>
			<!--	<li><a href="index.php">Test-Index</a></li>
				<li><a href="index.php?controller=profile">Test-Profile</a></li> -->
				<li><a href="index.php?controller=courses">Courses</a></li>
				<li><a href="index.php">Streams</a></li>
				<li><a href="index.php">About Us</a></li>
				<li><a href="index.php?controller=login">Login</a></li>
				<li><a href="index.php?controller=signup">Sign Up</a></li>
			</ul>
		</nav>
		<div id="lead-in">
		
		<h1>Feed Your Curiosity,<br>
				Take Online Courses from UWI</h1>

			<form name="course_search" method="post" action="index.php?controller=">
				<div class="wide-thick-bordered" >
				<input class="c-banner-search-input" type="text" name="formSearch" value="" placeholder="Find the right course for you">
				<button class="c-banner-search-button"></button>
				</div>
			</form> 
		</div>
		<header></header>
		<main>

			<h1>Most Popular</h1>
			
		<div class = "centered">		
			<section>
				<a href="#"><img src="images/<?php echo $PopImage[0] ?>" alt="<?php echo $PopCourse[0] ?>" title="<?php echo $PopCourse[0] ?>">
				<span class="course-title"><?php echo $PopCourse[0] ?></span>
				<span><?php echo $PopInstruct[0] ?></span></a>
			</section>
			
			
			<section>
				<a href="#"><img src="images/<?php echo $PopImage[1] ?>" alt="<?php echo $PopCourse[1] ?>" title="<?php echo $PopCourse[1] ?>">
				<span class="course-title"><?php echo $PopCourse[1] ?></span>
				<span><?php echo $PopInstruct[1] ?></span></a>
			</section>


			<section>
				<a href="#"><img src="images/<?php echo $PopImage[2] ?>" alt="<?php echo $PopCourse[2] ?>" title="<?php echo $PopCourse[2] ?>">
				<span class="course-title"><?php echo $PopCourse[2] ?></span>
				<span><?php echo $PopInstruct[2] ?></span></a>
			</section>


			<section>
				<a href="#"><img src="images/<?php echo $PopImage[3] ?>" alt="<?php echo $PopCourse[3] ?>" title="<?php echo $PopCourse[3] ?>">
				<span class="course-title"><?php echo $PopCourse[3] ?></span>
				<span><?php echo $PopInstruct[3] ?></span></a>
			</section>
		</div>


		<div class = "centered">		
			<section>
				<a href="#"><img src="images/<?php echo $PopImage[4] ?>" alt="<?php echo $PopCourse[4] ?>" title="<?php echo $PopCourse[4] ?>">
				<span class="course-title"><?php echo $PopCourse[4] ?></span>
				<span><?php echo $PopInstruct[4] ?></span></a>
			</section>
			
			
			<section>
				<a href="#"><img src="images/<?php echo $PopImage[5] ?>" alt="<?php echo $PopCourse[5] ?>" title="<?php echo $PopCourse[5] ?>">
				<span class="course-title"><?php echo $PopCourse[5] ?></span>
				<span><?php echo $PopInstruct[5] ?></span></a>
			</section>


			<section>
				<a href="#"><img src="images/<?php echo $PopImage[6] ?>" alt="<?php echo $PopCourse[6] ?>" title="<?php echo $PopCourse[6] ?>">
				<span class="course-title"><?php echo $PopCourse[6] ?></span>
				<span><?php echo $PopInstruct[6] ?></span></a>
			</section>


			<section>
				<a href="#"><img src="images/<?php echo $PopImage[7] ?>" alt="<?php echo $PopCourse[7] ?>" title="<?php echo $PopCourse[7] ?>">
				<span class="course-title"><?php echo $PopCourse[7] ?></span>
				<span><?php echo $PopInstruct[7] ?></span></a>
			</section>
		</div>





			<h1>Learner Recommended</h1>
			<div class="centered">
				
			
				<section>
				<a href="#"><img src="images/<?php echo $RecomImage[0] ?>" alt="<?php echo $RecomCourse[0] ?>" title="<?php echo $RecomCourse[0] ?>">
				<span class="course-title"><?php echo $RecomCourse[0] ?></span>
				<span><?php echo $RecomInstruct[0] ?></span></a>
				</section>


				<section>
				<a href="#"><img src="images/<?php echo $RecomImage[1] ?>" alt="<?php echo $RecomCourse[1] ?>" title="<?php echo $RecomCourse[1] ?>">
				<span class="course-title"><?php echo $RecomCourse[1] ?></span>
				<span><?php echo $RecomInstruct[1] ?></span></a>
				</section>		
				
				
				
				<section>
				<a href="#"><img src="images/<?php echo $RecomImage[2] ?>" alt="<?php echo $RecomCourse[2] ?>" title="<?php echo $RecomCourse[2] ?>">
				<span class="course-title"><?php echo $RecomCourse[2] ?></span>
				<span><?php echo $RecomInstruct[2] ?></span></a>
				</section>
				
				
				
				<section>
				<a href="#"><img src="images/<?php echo $RecomImage[3] ?>" alt="<?php echo $RecomCourse[3] ?>" title="<?php echo $RecomCourse[3] ?>">
				<span class="course-title"><?php echo $RecomCourse[3] ?></span>
				<span><?php echo $RecomInstruct[3] ?></span></a>
				</section>

			</div>
			<div class="centered">
			
			<section>
				<a href="#"><img src="images/<?php echo $RecomImage[4] ?>" alt="<?php echo $RecomCourse[4] ?>" title="<?php echo $RecomCourse[4] ?>">
				<span class="course-title"><?php echo $RecomCourse[4] ?></span>
				<span><?php echo $RecomInstruct[4] ?></span></a>
				</section>
			
			
			<section>
			<section>
				<a href="#"><img src="images/<?php echo $RecomImage[5] ?>" alt="<?php echo $RecomCourse[5] ?>" title="<?php echo $RecomCourse[5] ?>">
				<span class="course-title"><?php echo $RecomCourse[5] ?></span>
				<span><?php echo $RecomInstruct[5] ?></span></a>
				</section>
			</section>
			
			
			<section>
				<a href="#"><img src="images/<?php echo $RecomImage[6] ?>" alt="<?php echo $RecomCourse[6] ?>" title="<?php echo $RecomCourse[6] ?>">
				<span class="course-title"><?php echo $RecomCourse[6] ?></span>
				<span><?php echo $RecomInstruct[6] ?></span></a>
				</section>
			
			
				<section>
				<a href="#"><img src="images/<?php echo $RecomImage[7] ?>" alt="<?php echo $RecomCourse[7] ?>" title="<?php echo $RecomCourse[7] ?>">
				<span class="course-title"><?php echo $RecomCourse[7] ?></span>
				<span><?php echo $RecomInstruct[7] ?></span></a>
				</section>
			
		
			</div>
			<footer>
				<nav>
					<ul>
						<li>&copy;2018 Quwius Inc.</li>
						<li><a href="#">Company</a></li>
						<li><a href="#">Connect</a></li>
						<li><a href="#">Terms &amp; Conditions</a></li>
					</ul>
				</nav>
			</footer>
		</main>
	</body>
</html>