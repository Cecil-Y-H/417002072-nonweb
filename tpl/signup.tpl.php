<!DOCTYPE html>
<html lang="en-GB">
	<head>
		<title>Sign Up|Quwius</title>
		<link rel="stylesheet" href="css/styles.css" type="text/css" media="screen">
		<meta charset="utf-8">
		
		<style>
			.ErrorMessage {color: #FF0000;}
		</style>
		
		
	</head>
	<body>
	
	<?php
			//session_start();
			$_SESSION['valid'] = 1;
			
	?>
	
	
		<nav> 
			<a href="index.php"><img src="images/logo.png" alt="UWI online"></a> 
			<ul>
				<!-- <li><a href="index.php">Test-Index</a></li> -->
				<!-- <li><a href="index.php?controller=profile">Test-Profile</a></li> -->

				<li><a href="index.php?controller=courses">Courses</a></li>
				<li><a href="index.php?controller=signup">Streams</a></li>
				<li><a href="index.php?controller=signup">About Us</a></li>
				<li><a href="index.php?controller=login">Login</a></li>
				<li><a href="index.php?controller=signup">Sign Up</a></li>
			</ul>
		</nav>
		<main>
		   <div class="register-box">
			<div class="register-box-body">
			<p class="login-box-msg">Sign Up - Feed Your Curiosity</p>
        
		<form  
		action="index.php?controller=signup"
		method="post"
		autocomplete = "off">
          
		  <div class="form-group has-feedback">
            <input
			id = "formFullName"
			type="text" 
			class="form-control" 
			name="formFullName" 
			placeholder="Full name"/>
			
			<span class = "ErrorMessage"><b>
			<?php 
			if (isset($errors['formFullName'])): echo $errors['formFullName']; endif; 
			?>
			</b></span>
			
			
          </div>
          
		  
		  <div class="form-group has-feedback">
            <input 
			id = "formEmail"
			type="text" 
			class="form-control" 
			name="formEmail" 
			placeholder="Email"/>
          
			<span class = "ErrorMessage"><b>
			<?php 
			if (isset($errors['formEmail'])): echo $errors['formEmail']; endif; 
			?>
			</b></span>
		  
		  </div>
          
		  
		  <div class="form-group has-feedback">
            <input 
			id = "formPassword"
			type="password" 
			class="form-control"
			name="formPassword" 
			placeholder="Password"/>
          
			<span class = "ErrorMessage"><b>
			<?php 
			if (isset($errors['formPassword'])): echo $errors['formPassword']; endif; 
			?>
			</b></span>
		  
		  
		  </div>
          
		  
		  <div class="form-group has-feedback">
            <input 
			id = "formRetype"
			type="password" 
			class="form-control" 
			name="formRetype" 
			placeholder="Retype password"/>
			
			<span class = "ErrorMessage"><b>
			<?php 
			if (isset($errors['formRetype'])): echo $errors['formRetype']; endif; 
			?>
			</b></span>
			
			
          </div>
          
		  
		  <div class="row">
            <div class="col-xs-8">    
              <div class="checkbox icheck">
                <label>
                  <input type="checkbox"> I agree to the <a href="#">terms</a>
                </label>
              </div>                        
            </div><!-- /.col -->
            <div class="col-xs-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
            </div><!-- /.col -->
          </div>
        </form>
       </div><!-- /.login-box-body -->
	  </div>
			<footer>
				<nav>
					<ul>
						<li>&copy;2015 Quwius Inc.</li>
						<li><a href="#">Company</a></li>
						<li><a href="#">Connect</a></li>
						<li><a href="#">Terms &amp; Conditions</a></li>
					</ul>
				</nav>
			</footer>		
		</main>
		<?php
		
			
		?>
		
		
	</body>
</html>