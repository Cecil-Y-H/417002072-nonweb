<!DOCTYPE html>
<html lang="en-GB">
	<head>
		<title>Quwius</title>
		<link rel="stylesheet" href="css/styles.css" type="text/css" media="screen">
		<meta charset="utf-8">
	</head>
	<body>
		<nav>
			<a href="index.php"><img src="images/logo.png" alt="UWI online"></a>
			<ul>
				<li><a href="index.php?controller=profile">Profile</a></li>
				<li><a href="index.php?controller=courses">Streams</a></li>
				<li><a href="index.php?controller=courses">About Us</a></li>
				<li><a href="index.php?controller=logout">Log Out</a></li>
				<li><a href="index.php?controller=signup">Sign Up</a></li>
			</ul>
		</nav>

		<?php
			//echo $_SESSION['LoggedIn'];
		?>

		<?php
			
			
			
		?>

			
			
		


		<main>
		<h1>Courses</h1>
		<ul class="course-list">
			
		<?php for($i = 0; $i < sizeof($Name); $i++) : ?>	
		
			<li><div>
				<a href="#">
						<img src="images/<?php echo $Image[$i]?>" alt="<?php echo $Name[$i]; ?>"></a>
				</div>
				<div>
				<a href="#">
				<span class="faculty-department">
					<?php 
						echo $Faculty[$i]; 
					?>
				</span>	
					<span class="course-title">
							<?php echo $Name[$i]; ?>
					</span>
					<span class="instructor">
						<b> <?php 
						echo $Instructor[$i]; 
						
						?> </b>
					</span></a>
				</div>
				<div>
					<p>Get Curious.</p>
					<a href="#" class="startnow-button startnow-btn">Start Now!</a>
				</div>
			</li>
			
		<?php endfor; ?>
			
			
			
			
			
			
			
			
		</ul>
			<footer>
				<nav>
					<ul>
						<li>&copy;2015 Quwius Inc.</li>
						<li><a href="#">Company</a></li>
						<li><a href="#">Connect</a></li>
						<li><a href="#">Terms &amp; Conditions</a></li>
					</ul>
				</nav>
			</footer>
		</main>
	</body>
</html>