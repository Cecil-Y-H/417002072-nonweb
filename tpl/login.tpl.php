<!DOCTYPE html>
<html lang="en-GB">
	<head>
		<title>Login|Quwius</title>
		<link rel="stylesheet" href="css/styles.css" type="text/css" media="screen">
		<meta charset="utf-8">
		
		<style>
			.ErrorMessage {color: #FF0000;}
		</style>
		
	</head>
	<body>
		<nav>
			<a href="index.php"><img src="images/logo.png" alt="UWI online"></a>
			<ul>
				<li><a href="index.php?controller=courses">Courses</a></li>
				<li><a href="index.php?controller=login">Streams</a></li>
				<li><a href="index.php?controller=login">About Us</a></li>
				<li><a href="index.php?controller=login">Login</a></li>
				<li><a href="index.php?controller=signup">Sign Up</a></li>
			</ul>
		</nav>
		<main>
		   <div class="login-box">
			<div class="login-box-body">
			<p class="login-box-msg">Be Curious - Sign In</p>
			
			
			<form 
			action="index.php?controller=login" 
			method="post"
			autocomplete = "off">
			
			  <div class="form-group has-feedback">
				
				<input
				id ="LoginEmail"
				type="text" 
				class="form-control" 
				name="LoginEmail"
				placeholder="Email"/>
				
				<span class = "ErrorMessage"><b>
				<?php 
					if (isset($errors['LoginEmail'])): echo $errors['LoginEmail']; endif; 
				?>
				</b></span>
				
			  </div>
			  
			  <div class="form-group has-feedback">
				
				<input 
				id = "LoginPassword"
				type="password" 
				class="form-control" 
				name = "LoginPassword"
				placeholder="Password"/>
			  
				<span class = "ErrorMessage"><b>
				<?php 
					if (isset($errors['LoginPassword'])): echo $errors['LoginPassword']; endif; 
				?>
				</b></span>
			  
			  
			  </div>
			  
			  
			  
			  <div class="row">
				<div class="col-xs-8">    
				  <div class="checkbox icheck">
					<label>
					  <input type="checkbox"> Remember Me
					</label>
				  </div>                        
				</div><!-- /.col -->
				<div class="col-xs-4">
				  <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
				</div><!-- /.col -->
			  </div>
			</form>
			
			
			<br>
			<a href="signup.php" class="text-center">Sign Up</a>
       </div><!-- /.login-box-body -->
	  </div>
			<footer>
				<nav>
					<ul>
						<li>&copy;2015 Quwius Inc.</li>
						<li><a href="#">Company</a></li>
						<li><a href="#">Connect</a></li>
						<li><a href="#">Terms &amp; Conditions</a></li>
					</ul>
				</nav>
			</footer>
		</main>
	</body>
</html>