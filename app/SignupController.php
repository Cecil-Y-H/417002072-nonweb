<?php

namespace MOOC\apps;

use MOOC\framework\CommandContext;
use MOOC\framework\PageControllerCommandAbstract;
use MOOC\framework\View;
use MOOC\framework\ObservableModel;
use MOOC\framework\ResponseHandler;
use MOOC\framework\SessionClass;
use MOOC\framework\Validation;

use MOOC\framework\NoticeHeader;
use MOOC\framework\NoticeState;
use MOOC\framework\NoticeLogger;
use MOOC\framework\WarningHeader;
use MOOC\framework\WarningState;
use MOOC\framework\WarningLogger;



//require 'Validation.php';

class SignupController extends PageControllerCommandAbstract
{
    public function run(string $request)
    {
		
		
		$this->model = $this->CreateModel();

		$this->view = $this->CreateView();

		$this->model->attach($this->view);

		$data = $this->model->getAll();

		$this->model->updateThechangedData($data);

		$this->model->notify();
		
		$valid = $_SESSION['valid'];

			$response = ResponseHandler::getInstance();
			$session = SessionClass::getInstance();
			$head = new NoticeHeader();
        	$state = new NoticeState();
        	$logger = new NoticeLogger();

        	$set = array("Page-Displayed");
        	$head->setEntries($set);

        	$set = array("The Signup Page was successfully accessed and displayed.");
        	$state->setEntries($set);
        
        	$time = date("h:i:sa");
        	$set = array($time);
        	$logger->setEntries($set);

        	$response->create($head, $state, $logger);
			$session->add("RESPONSE", $response);

		if( (!empty($_POST)) && $valid == 1 )
		{
			
			$validator = Validation::getInstance();
			$validator->fillData($_POST);
			$result = $validator->signupValidate();
			
			if(!$result)
			{
				echo "<br>";
				echo "Please see your Errors below!\n";
				$errors = $validator->getErrors();
				$this->view->setTemplate(TPL_DIR . '/signup.tpl.php');
				$this->view->addVar('errors', $errors);
				$this->view->display();

					$head = new WarningHeader();
        			$state = new WarningState();
        			$logger = new WarningLogger();

        			$set = array("Invalid-Data");
        			$head->setEntries($set);

        			$set = array("An Invalid Set of Data was entered at the Sign Up Page");
        			$state->setEntries($set);
        
        			$time = date("h:i:sa");
        			$set = array($time);
        			$logger->setEntries($set);

        			$response->create($head, $state, $logger);
					$session->add("RESPONSE", $response);

					
			}
			
			else
			{
					$head = new NoticeHeader();
        			$state = new NoticeState();
        			$logger = new NoticeLogger();

        			$set = array("Signup-Success");
        			$head->setEntries($set);

        			$set = array("Sign Up Successful. Proceeding to Login Page");
        			$state->setEntries($set);
        
        			$time = date("h:i:sa");
        			$set = array($time);
        			$logger->setEntries($set);

        			$response->create($head, $state, $logger);
					$session->add("RESPONSE", $response);
					$this->model->update($_POST);
					

				header('Location:index.php?controller=login');
			}	
		}
		
	}


	public function CreateModel() : ObservableModel
	{
		return new SignupModel();
	}

	public function CreateView() : View
	{
		$view = new View();
		$view->setTemplate(TPL_DIR . '/signup.tpl.php');
		return $view;
	}



		
	public function execute(CommandContext $context) : bool
	{
		$contextData = $context->get('get');
		$newRequest = $contextData['controller'];

		

		$this->run($newRequest);
		return true;
	}
    

}