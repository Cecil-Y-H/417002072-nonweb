<?php

namespace MOOC\apps;


use MOOC\framework\ObservableModel;
use MOOC\framework\InsertTrait;
    

class IndexModel extends ObservableModel
{
    use InsertTrait;

    public function getAll() : array
    {
        //echo "Get All invoked!";
        
        $connection = ObservableModel::makeConnection();

        //var_dump($connection);

        $POPULAR = "select courses.course_name, courses.course_image, instructors.instructor_name, courses.course_access_count from courses, course_instructor, instructors where courses.course_id = course_instructor.course_id and course_instructor.course_id = course_instructor.instructor_id and course_instructor.instructor_id = instructors.instructor_id ORDER BY courses.course_access_count DESC";
        
        $result = $connection->query($POPULAR);

        $popCourse = [];
        $popInstruct = [];
        $popImage = [];
        

        //var_dump($result);
        if($result->num_rows > 0)
        {
            while($row = $result->fetch_assoc())
            {
            
                array_push($popCourse, $row["course_name"]);
                array_push($popInstruct, $row["instructor_name"]);
                array_push($popImage, $row["course_image"]);

            }
        }

        $popCourse = array_slice($popCourse, 0, 8);
        $popInstruct = array_slice($popInstruct, 0, 8);
        $popImage = array_slice($popImage, 0, 8);

        
        $RECOMMENDED = "select courses.course_name, courses.course_image, instructors.instructor_name, courses.course_recommendation_count from courses, course_instructor, instructors where courses.course_id = course_instructor.course_id and course_instructor.course_id = course_instructor.instructor_id and course_instructor.instructor_id = instructors.instructor_id ORDER BY courses.course_recommendation_count DESC";
        
        $result = $connection->query($RECOMMENDED);

        $RecomCourse = [];
        $RecomInstruct = [];
        $RecomImage = [];

        if($result->num_rows > 0)
        {
            while($row = $result->fetch_assoc())
            {
            
                array_push($RecomCourse, $row["course_name"]);
                array_push($RecomInstruct, $row["instructor_name"]);
                array_push($RecomImage, $row["course_image"]);

            }
        }

        $RecomCourse = array_slice($RecomCourse, 0, 8);
        $RecomInstruct = array_slice($RecomInstruct, 0, 8);
        $RecomImage = array_slice($RecomImage, 0, 8);
        //var_dump($RecomCourse);
        //echo ($RecomImage[0]);

        
        return ['PopCourse'=>$popCourse, 
                'PopInstruct'=>$popInstruct, 
                'PopImage'=>$popImage,

                'RecomCourse'=>$RecomCourse,
                'RecomInstruct'=>$RecomInstruct,
                'RecomImage'=>$RecomImage
            ];
    }

    public function getRecord(string $id) : array
    {
        return [];
    }

    public function insert(array $values)
    {
        echo "Insert Trait Method still works properly!";
    }


}