<?php

namespace MOOC\apps;

use MOOC\framework\CommandContext;
use MOOC\framework\PageControllerCommandAbstract;
use MOOC\framework\View;
use MOOC\framework\ObservableModel;
use MOOC\framework\ResponseHandler;
use MOOC\framework\Validation;
use MOOC\framework\SessionClass;

use MOOC\framework\NoticeHeader;
use MOOC\framework\NoticeState;
use MOOC\framework\NoticeLogger;
use MOOC\framework\WarningHeader;
use MOOC\framework\WarningState;
use MOOC\framework\WarningLogger;


class LoginController extends PageControllerCommandAbstract
{
	public function run(string $request)
	{
		
		$this->model = $this->CreateModel();

		$this->view = $this->CreateView();
		
		$this->model->attach($this->view);
		
		$data = $this->model->getAll();
		
		$this->model->updateThechangedData($data);
		
		$this->model->notify();
		

			$response = ResponseHandler::getInstance();
			$session = SessionClass::getInstance();
			$head = new NoticeHeader();
			$state = new NoticeState();
			$logger = new NoticeLogger();

			$set = array("Page-Displayed");
			$head->setEntries($set);

			$set = array("The Log In Page was successfully accessed and displayed.");
			$state->setEntries($set);
	
			$time = date("h:i:sa");
			$set = array($time);
			$logger->setEntries($set);

			$response->create($head, $state, $logger);
			$session->add("RESPONSE", $response);

		if(!empty($_POST))
		{
			$validator = Validation::getInstance();
			$validator->fillData($_POST);
			$result = $validator->loginValidate();
			
			
			if(!$result)
			{
				echo "<br>";
				echo "Please see your Errors below!\n";
				$errors = $validator->getErrors();
				$this->view->addVar('errors', $errors);
				$this->view->display();


					$head = new WarningHeader();
					$state = new WarningState();
					$logger = new WarningLogger();

					$set = array("Invalid-Data");
					$head->setEntries($set);

					$set = array("An Invalid Set of Data was entered at the Login Page");
					$state->setEntries($set);
	
					$time = date("h:i:sa");
					$set = array($time);
					$logger->setEntries($set);

					$response->create($head, $state, $logger);
					$session->add("RESPONSE", $response);

					
			}
			
			else
			{
				$email = $_POST['LoginEmail'];
				//var_dump($email);

				$record_array = $this->model->getRecord($email);
				
				//var_dump($record_array);
				
				if($this->model->verifyPassword($record_array))
				{
					$login = SessionClass::getInstance();
					$login->create();
					
						$head = new NoticeHeader();
        				$state = new NoticeState();
        				$logger = new NoticeLogger();

        				$set = array("Login-Success");
        				$head->setEntries($set);

        				$set = array("Login Up Successful. Proceeding to Profile Page");
        				$state->setEntries($set);
        
        				$time = date("h:i:sa");
        				$set = array($time);
        				$logger->setEntries($set);

        				$response->create($head, $state, $logger);
						$login->add("RESPONSE", $response);
					//$this->model->update($_POST);

						$login->add('LoggedIn', $email);

						echo $head->getEntry(0); echo "<br>";
        				echo $state->getEntry(0); echo "<br>";
        				echo $logger->getEntry(0); echo "<br><br>";
						header('Location:index.php?controller=profile');
				}
				
				else;
				
			}
			
			$head = new WarningHeader();
        	$state = new WarningState();
			$logger = new WarningLogger();

        	$set = array("No-Credentials");
        	$head->setEntries($set);

        	$set = array("Entered Credentials were not in the Database");
        	$state->setEntries($set);
        
        	$time = date("h:i:sa");
        	$set = array($time);
        	$logger->setEntries($set);

        	$response->create($head, $state, $logger);
			$session->add("RESPONSE", $response);

			//echo "Please enter an email and password!";
		}
	}


	public function CreateModel() : ObservableModel
	{
		return new LoginModel();
	}

	public function CreateView() : View
	{
		$view = new View();
		$view->setTemplate(TPL_DIR . '/login.tpl.php');
		return $view;
	}




	public function execute(CommandContext $context) : bool
	{
		$contextData = $context->get('get');
		$newRequest = $contextData['controller'];

		//echo $newRequest;

		$this->run($newRequest);

		return true;
	}

}


