<?php

namespace MOOC\apps;


use MOOC\framework\CommandContext;
use MOOC\framework\PageControllerCommandAbstract;
use MOOC\framework\View;
use MOOC\framework\ObservableModel;
use MOOC\framework\SessionClass;
use MOOC\framework\ResponseHandler;

use MOOC\framework\NoticeHeader;
use MOOC\framework\NoticeState;
use MOOC\framework\NoticeLogger;
use MOOC\framework\WarningHeader;
use MOOC\framework\WarningState;
use MOOC\framework\WarningLogger;

class ProfileController extends PageControllerCommandAbstract 
{
    public function run(string $request)
    {
		
        $response = ResponseHandler::getInstance();
        $session = SessionClass::getInstance();
        $session->create();
        $user = $session->see('LoggedIn');
        //echo $user; echo "<br>";
        $page = 'profile';

        
        if ($session->accessible($user, $page))
        {
           // echo "You are allowed to access this page!";
        }

        else
        {   
            $head = new WarningHeader();
            $state = new WarningState();
            $logger = new WarningLogger();

            $set = array("Bad-Access");
            $head->setEntries($set);

            $set = array("Unauthorized Attempt to access Profile Page. Redirecting to Index .");
            $state->setEntries($set);
        
            $time = date("h:i:sa");
            $set = array($time);
            $logger->setEntries($set);

            $response->create($head, $state, $logger);
            $session->add("RESPONSE", $response);
            
            
            header('Location:index.php');
        }

        $this->model = $this->CreateModel();

		$this->view = $this->CreateView();

        $this->model->makeConnection();

        $this->model->attach($this->view);
        
        $data = $this->model->read($user);

        $this->model->updateThechangedData($data);

        $this->model->notify();

            
            $head = new NoticeHeader();
            $state = new NoticeState();
            $logger = new NoticeLogger();

            $set = array("Page-Displayed");
            $head->setEntries($set);

            $set = array("The Profile Page was successfully accessed and displayed.");
            $state->setEntries($set);
        
            $time = date("h:i:sa");
            $set = array($time);
            $logger->setEntries($set);

            $response->create($head, $state, $logger);
            $session->add("RESPONSE", $response);

    }


    public function CreateModel() : ObservableModel
	{
		return new ProfileModel();
	}

	public function CreateView() : View
	{
		$view = new View();
        $view->setTemplate(TPL_DIR . '/profile.tpl.php');
		return $view;
	}
    





    public function execute(CommandContext $context) : bool
    {   
        $contextData = $context->get('get');
        $newRequest = $contextData['controller'];
        //var_dump($context);
        //echo "<br><br><br><br><br>";
        //echo $newRequest;
        
        $this->run($newRequest);
        return true;
    }

}