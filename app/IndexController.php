<?php

namespace MOOC\apps;

use MOOC\framework\CommandContext;
use MOOC\framework\ObservableModel;
use MOOC\framework\PageControllerCommandAbstract;
use MOOC\framework\ResponseHandler;
use MOOC\framework\SessionClass;
use MOOC\framework\View;

use MOOC\framework\NoticeHeader;
use MOOC\framework\NoticeState;
use MOOC\framework\NoticeLogger;

class IndexController extends PageControllerCommandAbstract 
{
    //Stores context data from the execute method
    //public $contextData = null;

    public function run(string $request)
    { 
        $this->model = $this->CreateModel();
      
        $this->view = $this->CreateView();

        $this->model->makeConnection();

        $this->model->attach($this->view);

        $data = $this->model->getAll();
    
        $this->model->updateThechangedData($data);

        $this->model->notify();

        $response = ResponseHandler::getInstance();
        $session = SessionClass::getInstance();
       
        $head = new NoticeHeader();
        $state = new NoticeState();
        $logger = new NoticeLogger();

        $set = array("Page-Displayed");
        $head->setEntries($set);

        $set = array("The Index Page was successfully accessed and displayed.");
        $state->setEntries($set);
        
        $time = date("h:i:sa");
        $set = array($time);
        $logger->setEntries($set);

        $response->create($head, $state, $logger);



        /*   
        $set = array("Page-Displayed-AGAIN");
        $head->setEntries($set);

        $set = array("The Index Page was successfully accessed and displayed AGAIN.");
        $state->setEntries($set);
        
        $time = date("h:i:sa");
        $set = array($time);
        $logger->setEntries($set);
        
        $head->getEntries(0, 1);
        $state->getEntries(0, 1);
        $logger->getEntries(0, 1);
        */

        /*
        $response->create($head, $state, $logger);

        echo $head->getEntry(0); echo "<br>";
        echo $state->getEntry(0); echo "<br>";
        echo $logger->getEntry(0); echo "<br><br>";

        echo $head->getEntry(1); echo "<br>";
        echo $state->getEntry(1); echo "<br>";
        echo $logger->getEntry(1); echo "<br>";
        */ 
       
        $session->add("RESPONSE", $response);
        
    }

    public function CreateModel() : ObservableModel
    {
        return new IndexModel();
    }

    public function CreateView() : View
    {
        $view = new View();
        $view->setTemplate(TPL_DIR . '/index.tpl.php');
        return $view;
    }




    public function execute(CommandContext $context) : bool
    {   
        $contextData = $context->get('get');
        $newRequest = $contextData['controller'];

        if(empty($newRequest))
        {
            $newRequest = 'index';
        }
        //var_dump($context);
        //echo "<br><br><br><br><br>";
        //var_dump($contextData);
        
        $this->run($newRequest);
        return true;
    }
}